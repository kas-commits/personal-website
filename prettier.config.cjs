module.exports = {
  arrowParens: "avoid",
  singleQuote: true,
  trailingComma: "all",
  proseWrap: "always",
};
