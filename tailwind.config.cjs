const defaultTheme = require("tailwindcss/defaultTheme");
const colors = require("tailwindcss/colors");

const cprime = colors.blue;
const grayscale = colors.slate;
const cwhite = colors.white;

const prose_colors = {
  body: grayscale[700],
  headings: grayscale[900],
  lead: grayscale[600],
  links: cprime[600],
  bold: grayscale[900],
  counters: grayscale[900],
  bullets: grayscale[300],
  hr: grayscale[200],
  quotes: grayscale[900],
  quote_borders: grayscale[200],
  captions: grayscale[500],
  code: grayscale[900],
  pre_code: grayscale[200],
  pre_bg: grayscale[800],
  th_borders: grayscale[300],
  td_borders: grayscale[200],
  invert_body: grayscale[300],
  invert_headings: cwhite,
  invert_lead: grayscale[400],
  invert_links: cprime[500],
  invert_bold: cwhite,
  invert_counters: grayscale[400],
  invert_bullets: grayscale[600],
  invert_hr: grayscale[700],
  invert_quotes: grayscale[100],
  invert_quote_borders: grayscale[700],
  invert_captions: grayscale[400],
  invert_code: colors.white,
  invert_pre_code: grayscale[300],
  invert_pre_bg: "rgb(0 0 0 / 50%)",
  invert_th_borders: grayscale[600],
  invert_td_borders: grayscale[700],
};

const grayscale_css_colors = {
  css: {
    "--tw-prose-body": prose_colors.body,
    "--tw-prose-headings": prose_colors.headings,
    "--tw-prose-lead": prose_colors.lead,
    "--tw-prose-links": prose_colors.links,
    "--tw-prose-bold": prose_colors.bold,
    "--tw-prose-counters": prose_colors.counters,
    "--tw-prose-bullets": prose_colors.bullets,
    "--tw-prose-hr": prose_colors.hr,
    "--tw-prose-quotes": prose_colors.quotes,
    "--tw-prose-quote-borders": prose_colors.quote_borders,
    "--tw-prose-captions": prose_colors.captions,
    "--tw-prose-code": prose_colors.code,
    "--tw-prose-pre-code": prose_colors.pre_code,
    "--tw-prose-pre-bg": prose_colors.pre_bg,
    "--tw-prose-th-borders": prose_colors.th_borders,
    "--tw-prose-td-borders": prose_colors.td_borders,
    "--tw-prose-invert-body": prose_colors.invert_body,
    "--tw-prose-invert-headings": prose_colors.invert_headings,
    "--tw-prose-invert-lead": prose_colors.invert_lead,
    "--tw-prose-invert-links": prose_colors.invert_links,
    "--tw-prose-invert-bold": prose_colors.invert_bold,
    "--tw-prose-invert-counters": prose_colors.invert_counters,
    "--tw-prose-invert-bullets": prose_colors.invert_bullets,
    "--tw-prose-invert-hr": prose_colors.invert_hr,
    "--tw-prose-invert-quotes": prose_colors.invert_quotes,
    "--tw-prose-invert-quote-borders": prose_colors.invert_quote_borders,
    "--tw-prose-invert-captions": prose_colors.invert_captions,
    "--tw-prose-invert-code": prose_colors.invert_code,
    "--tw-prose-invert-pre-code": prose_colors.invert_pre_code,
    "--tw-prose-invert-pre-bg": prose_colors.invert_pre_bg,
    "--tw-prose-invert-th-borders": prose_colors.invert_th_borders,
    "--tw-prose-invert-td-borders": prose_colors.invert_td_borders,
  },
};

module.exports = {
  content: ["./src/**/*.{astro,html,js,jsx,svelte,ts,tsx}"],
  theme: {
    extend: {
      typography: {
        DEFAULT: {
          css: [grayscale_css_colors.css],
        },
      },
      colors: {
        primary: {
          content: cprime[700],
          focus: cprime[800],
          icontent: cprime[400],
          ...cprime,
        },
        cprose: prose_colors,
        base: {
          icontent: grayscale[100],
          accent: grayscale[200],
          focus: grayscale[300],
          content: grayscale[800],
          ...grayscale,
        },
      },
      fontFamily: {
        serif: ["DM Mono", ...defaultTheme.fontFamily.serif],
        sans: ["DM Sans", ...defaultTheme.fontFamily.sans],
        mono: ["Fira CodeVariable", ...defaultTheme.fontFamily.mono],
      },
    },
  },

  plugins: [require("@tailwindcss/typography")],
};
