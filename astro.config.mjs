import { defineConfig } from "astro/config";
import sitemap from "@astrojs/sitemap";
import tailwind from "@astrojs/tailwind";
import remarkMath from "remark-math";
// import remarkGfm from 'remark-gfm';
// import remarkSmartypants from 'remark-smartypants';
import { remarkReadingTime } from "./remark-reading-time.mjs";
import rehypeSlug from "rehype-slug";
// import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeFigure from "rehype-figure";
import rehypeAddClasses from "rehype-add-classes";
import rehypeKatex from "rehype-katex";
// import { h, s } from 'hastscript';

// https://astro.build/config
export default defineConfig({
  experimental: {},
  trailingSlash: "ignore",
  outDir: "./target",
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
    routing: {
      prefixDefaultLocale: false,
    },
  },
  markdown: {
    syntaxHighlight: "shiki",
    gfm: true,
    smartypants: true,
    remarkPlugins: [
      // [remarkGfm, {}],
      // [remarkSmartypants, {}],
      [remarkMath, {}],
      [remarkReadingTime, {}],
    ],
    rehypePlugins: [
      rehypeKatex,
      rehypeSlug,
      rehypeFigure,
      [
        rehypeAddClasses,
        {
          "h1,h2,h3": "article--title",
          p: "article-p",
          code: "article-code",
          h3: "article-h3",
          h2: "article-h2",
          h1: "article-h1",
          ol: "article-ol",
          ul: "article-ul",
          li: "article-li",
          table: "article-table",
          thead: "article-thead",
          tbody: "article-tbody",
          blockquote: "article-blockquote",
          figure: "article-figure",
          img: "article-img",
          figcaption: "article-figcaption",
          a: "article-a",
        },
      ],
    ],
    shikiConfig: {
      // Choose from Shiki's built-in themes (or add your own)
      // https://github.com/shikijs/shiki/blob/main/docs/themes.md
      // theme: 'dark-plus',
      theme: "catppuccin-mocha",
      // Add custom languages
      // Note: Shiki has countless langs built-in, including .astro!
      // https://github.com/shikijs/shiki/blob/main/docs/languages.md
      langs: [],
      // Enable word wrap to prevent horizontal scrolling
      wrap: true,
    },
  },
  site: "https://karemsamad.com/",
  prefetch: true,
  integrations: [tailwind(), sitemap()],
});
