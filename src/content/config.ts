import { z, defineCollection } from "astro:content";

const blogCollection = defineCollection({
  schema: ({ image }) =>
    z.object({
      title: z.string(),
      // pubDate: z.string().transform(str => new Date(str)),
      pubDate: z.date(),
      tags: z.array(z.string()),
      subtitle: z.string().optional(),
      description: z.string().optional(),
      cover: image().optional(),
      sourceLink: z.string().optional(),
      caption: z.string().optional(),
      source: z.string().optional(),
      draft: z.boolean().optional().default(false),
      short: z.boolean().default(false),
      toc: z.boolean().default(true),
    }),
});

export const collections = {
  blog: blogCollection,
};
