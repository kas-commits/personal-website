---
title: "Oops, I Did It Again: My Yearly Website Redesign"
subtitle: Hard Lessons Learned After a Year of self-hosted blogging
description:
  After a year of learning html, css, JavaScript from a classical CS starting
  point, what lessons have I learned the hard way and what pitfalls still exist
  in the space?
pubDate: 2023-01-29
cover: ./preview.jpg
tags:
  - Web Development
  - Astro
draft: true
---

Just over a year ago I registered the rights to `karemsamad.com` as a domain and
embarked on what I thought would be a simple journey into learning web
development.

I thought that after tackling the challenges of numerical programming, web
development should be a piece of cake, right? After all, with so many web apps
available these days, the industry would have figured it out by now and
consolodated behind an industry standard ui framework. right? RIGHT?!

> Yea... no

Okay, in reality it's not **_all_** chaos and tortue, but when a industry sector
as big as th

## JavaScript is weird

JavaScript is one of those languages that's popular because of its applications
rather than its merits. C will never die because its embeded itself into the API
of our operating systems. Python will never die because of the infinite number
of powerful libraries. Java will never die even though it really should and
would have died were it not for the plague of middle management and their
eternal love of proprietary BS like [SAP](https://www.sap.com).

> Seriously though, please do your part in collecting the real-life garbage that
> is Java. Say no to Java.

![](/assets/blog/astro2/twitter_1.jpg)

## Keep It Simple, Stupid!

## Pictures say two words: more readers

## Hosting a (static) site is rediculously easy

I started off by writing pure html and css at first.

## Designing is (much) harder than implementing

I had this great idea for my website where I would style the headers to look
like a command prompt for a terminal emulator. Not being a frontend developer,
it took me multiple hours to figure out how to best get those iconic 3 buttons
on the top left of MacOS apps

## Astro: the new PHP of the web

When you first learn about web pages you are advised to write pure html files as
your form of markup[^1]. While partaking in this exercise you quickly realise

1. You cannot (easily) generate different web pages which could change based on
   the HTTP request, and
2. You cannot reuse or compose sections of content across multiple pages/files.

When Rasmus Lerdorf announced version 1.0

## Tailwind is more powerful than you think

## CDNS rule the world

## Don't ignore your semantic html tags

[^1]: To be clear, I also advise those starting out to do this, even today.
