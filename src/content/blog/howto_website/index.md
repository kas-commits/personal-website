---
title: My Journey to Building a Website
pubDate: 2022-03-19
tags:
  - Web Development
short: true
---

One of the most fun parts of doing a project is sharing the learning experience
that comes with it. As a plus I love talking about myself, so I thought I'd
dedicate a new post to explain the main bits and pieces to this personal
website.

### Preliminaries

Before getting started you're going to need a few things ready. Namely, you'll
need:

- A domain name (that's the thing people type in the URL box to reach your
  website)
- A server in the backend that's setup to serve sites

Strictly speaking, the domain name isn't necessary. But telling people to visit
"172.105.19.157" every time won't get you many visitors. There's a lot (and I
really do mean a lot) of organizations that provide domain name registration
services. It doesn't matter which one you pick, but do note that many content
hosts offer domain name services already, so it might be better to choose how
you will host your website before you buy a domain name.

Choosing the server is a lot more involved of a process. There's two main ways
you can go about setting up the server. The first, and much simpler method, is
to chose a host which sets up everything for you. [Wix](https://www.wix.com),
[Squarespace](https://www.squarespace.com), [Ghost](https://ghost.org) and many
others exist and are well-priced for what they offer. The second, more involved
method, is to use a host which will simply serve your HTML, CSS and Javascript
"as-is." While the first method is much quicker to setup, the second method is
cheaper, more flexible and a great learning experience.

For the server I went with [Linode](https://www.linode.com) because I was
following a guide and wasn't aware of [Netlify](https://www.netlify.com) at the
time. Be warned that if you go the Linode route you should be pretty confident
with controlling a headless Linux box over SSH. If you're not already
comfortable with bash and the core utility commands on Linux then going the
Linode route might not be best. If I was handing out a recommendation between
the two, I would definitely go with Netlify as long as you don't need a custom
or boutique backend (which in all likelihood you won't). In fact, Netlify is so
developer-friendly, all you need to do is point it to your
Github/Gitlab/Gitbucket repo and it will automatically build and deploy the site
for you anytime you push to the master branch. And you don't even need to have a
credit card on record to use the free tier! That's some serious convenience.

### Now for the meat and potatoes

Now that we've got the boring stuff out of the way, it's time for the actual
application code! The first step for this section is choosing a framework. I've
barely written any front end code and I can already tell you that going
framework-less is not a good call for most cases. Frameworks let you do stuff
like write reusable components, combine HTML and Javascript more seamlessly,
handle complex routing and so much more. While it's possible to do all of that
without a framework, it won't be easy or fun (and it will probably perform worse
if you try hand-rolling your own version of the larger infrastructure).

There are many frameworks out there, but the main ones to consider are React,
Vue and Angular. [React](https://reactjs.org) is backed by Facebook and is
currently the most popular by a long shot. [Angular](https://angular.io) is
google's framework and is the oldest of the three. Due to its age it's the most
fleshed out, which also means it has the most cruft of the three. From what I've
heard from the webdev community it seems to perform a bit slower than the rest.
For a totally unbiased, unopinionated review of Angular I would point you to
[Ben Awad](https://www.youtube.com/watch?v=Cy3viKuo6vE).
[Vue](https://vuejs.org) is a little more interesting as it's community driven.
The creator of Vue is also the main author of [Vite](https://vitejs.dev), which
is a build tool that quite honestly defies gravity. This a point I'll touch on
later but I'm dumbfounded with how good the tooling is in the webdev community.

If none of those sound interesting to you than maybe I could interest you the
new kid on the block. [Svelte](https://svelte.dev) is the framework that I went
for and so far I'm very pleased with my choice. It seems to have learned a lot
from the missteps of the frameworks that came before it. It's also the
[most loved framework currently](https://insights.stackoverflow.com/survey/2021?utm_source=thenewstack&utm_medium=website&utm_campaign=platform#most-loved-dreaded-and-wanted-webframe-love-dread).
If you're going to use Svelte you should check out SvelteKit which provides all
the goodies outside the Svelte compiler like server side routing.

### The extras

There's a lot of additional goodies you can add to your project to improve the
quality of life of the app. Node's native package manager, npm, has tons of
available packages. You can add support for many tools through SvelteKit's
config file. You can also add hooks for preprocessors like
[mdsvex](https://mdsvex.com) and[Tailwindcss](https://tailwindcss.com), both of
which I've made extensive use of in this project.
