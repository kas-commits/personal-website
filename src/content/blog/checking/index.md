---
title: Taming Misbehaved Numerical Programs
subtitle: Please don't log a 2Gb tensor. It won't end well for anyone involved.
description: |-
  When it comes to preventing errors in software development, should you rely
  on runtime checks and assertions or compile-time errors? Let's investigate
pubDate: 2023-03-25
cover: ./cover.jpg
tags:
  - Programming Languages
  - C++
draft: true
---

Life in the tech industry has been quite busy. A few banks were busy failing,
more companies succumbed to layoffs and my grandmother is asking me if AI will
take over my job (PS: [it won't](https://www.youtube.com/watch?v=7Ff99Ir78NI)).
Rather than

Something something, sidestepped

With the advent of Rust on the systems programming side along with
[tRPC](https://trpc.io/) and [Zod](https://zod.dev/) in the web-app side of
things, compile-time (or, more generally, build-time / deploy-time) error
checking has become a real hot topic lately. While I'm glad that the broader
industry has found the programming equivalent of drinking water in the form of
type theory, it's important to remember that not everything can be checked ahead
of time.

### Failure is good

Failure is too often seen in a negative light[^1]. But the defining trait of
failure is not only its silver lining but its redeeming quality. With the right
mindset and tools, failure leads to retrospection, which ultimately leads to a
deeper level of understanding. Bryan Cantrill once mentioned in
[a brilliant talk at DockerCon](https://youtu.be/AdMqCUhvRz8?t=1304):

> If your state has become corrupt, it is incumbant upon you to die and donate
> your body to science where it can be debugged.

It's a little... Intense, but at least it unabashedly gets the point across:
when you fail on your own terms you can level the playing field in the
post-mortem. Only one hiccup though: what does it look like when the state of a
numerical program is corrupt?

Debugging an improper numerical program might sound like debugging any other
program, but what outsiders fail to appreciate is that numerical debugging tools
are notoriously crappy. And it's not even their fault! How are you expected to
unit-test a neural network model's output when the whole point is that the model
is supposed to predict approximations for difficult questions. Well, not all is
lost in this traitorous domain, but you do need to know a few tricks. Luckily,
I'm here to be your tour guide.

### Know your hardware (again)

> I feel like I've
> [already written a blog post about this](/blog/posts/knowing_your_hardware),
> but perhaps one needs to see the same lesson in many forms before truly
> appreciating it.

Modern computer microarchitectures all implement instruction sets for floating
point arithmetic that follow the
[IEEE 754 standard](https://en.wikipedia.org/wiki/IEEE_754), which every
numerical programmer should be intimately familiar with. It defines the binary
format that floating point values take and special sentinel values to indicate
exceptions.

For example, did you know that NaN values have an IEEE floating point
representation? Did you _also_ know that positive **_and_** negative infinity
values exist? All of these values are sentinels because they are valid instances
of the data type, but they fundamentally prevent meaningful results to be
extracted once you get them. Having your floating point value be in the NaN
state is very much akin to having your pointer be in the `NULL` state: it's not
a type error, but if you don't handle that edge case you'll inevitably regret
it.

How well do you know your own hardware? Let's take a pop quiz to test out a few
edge cases. See if you can tell what the right output of this code should
be[^3]:

```cpp
auto a = (double)2.0 / (double)0.0;
auto b = (double)2.0 / (double)-0.0;
auto c = (double)0.0 / (double)0.0;
auto d = std::pow(b, (double)0.0);
auto e = std::pow((double)0.0, (double)0.0);
std::cout << (0.0 == -0.0) << ' '
          << (a == a) << ' '
          << (a == b) << ' '
          << (c == c) << ' '
          << (d == e) << ' ';
```

If you'd like to know more about the IEEE standard I highly recommend
[Micheal's blog post going over most of the spec](https://matloka.com/blog/floating-point-101)

### Help the compiler help you

The first actionable line of defense we can employ is asking the compiler to
enforce certain constraints on our data types. This part is compiler (And hence
programming-language) dependent, but I'll stick with C++ for now.

Here's an example function where we're given a single and double precision
values and we divide one by the other. I've used `auto` to deduce the return
type. The first question I'd like to pose to the reader is what assembly
instruction(s) will the compiler chose to carry out the division (assume an
x86_64 processor with MMX/AVX extensions)?

```cpp
auto foo(float a, double b) {
  return a / b;
}
```

Well, the C++ language spec actually forces a specific answer to this question.
In C++, the language specifies that operations between a float and a double
should be performed by promoting the float to a double and then performing the
operation. If we take a look at the compiled assembly we see that's exactly what
happened:

```asm
_Z3foofd:
    push        rbp
    mov         rbp, rsp
    movss       dword ptr [rbp - 4], xmm0
    movsd       qword ptr [rbp - 16], xmm1
    movss       xmm0, dword ptr [rbp - 4]       # xmm0 = mem[0],zero,zero,zero
    cvtss2sd    xmm0, xmm0
    divsd       xmm0, qword ptr [rbp - 16]
    pop         rbp
    ret
```

> In case you don't speak x86 the first 5 instructions are just setting up the
> the procedure and variables, `cvtss2sd` is an abbreviation of "convert scalar
> single to scalar double", `divsd` is "divide scalar double" and `pop` and
> `ret` just finalize the ending of the procedure.

So if the default behavior is to keep the return value as `double` then what
happens when I change the type of the function to instead return a `float`? Will
the compiler change its mind about the promotion and instead try "demotion"?
Compiling the code we get the following:

```asm
_Z3barfd:
    push        rbp
    mov         rbp, rsp
    movss       dword ptr [rbp - 4], xmm0
    movsd       qword ptr [rbp - 16], xmm1
    movss       xmm0, dword ptr [rbp - 4]       # xmm0 = mem[0],zero,zero,zero
    cvtss2sd    xmm0, xmm0
    divsd       xmm0, qword ptr [rbp - 16]
    cvtsd2ss    xmm0, xmm0
    pop         rbp
    ret
```

No, it doesn't! That's because it's an illegal optimization. Why? As I mentioned
earlier, the language spec clearly says that `a` should be promoted for the
division, so even though it's less efficient than using `divss` and `cvtsd2ss`,
it would be semantically different, which would be an illegal optimization.

> To the untrained eye it's not actually obvious why the second option would be
> semantically different: since we convert the `double` to a `float` don't we
> lose any extra precision from using `divsd` and get the same result? Indeed,
> they're not equivalent, because demotion may require rounding, and rounding is
> not associative.

### When the compiler isn't enough

Software does not simply exist in an ether. It's usually written to be placed on
top of a currently running system, designed so others may live above it. That is
the nature of composeable systems (an awesome thing, I might add). So in many
situations, having your code produce a bunch of NaNs or infs is simply not an
acceptable result, even though these values are completely valid type instances
of floating point values.

This issue occurs a lot for
[ill-conditioned numerical problems](https://en.wikipedia.org/wiki/Condition_number)
and can be a real pain when your boss isn't buying the whole "infs are valid
results" plea. So now it's your responsibility to hunt down every possible
numerically unstable code snippet corrupting all your precious floats faster
than a freshman frat house.

> This happens... a lot. It's not a very pleasant experience.

Take the following code for example. It seems simple enough, right?

[^1]:
    Having our performance evaluated based on numeric grade results for the
    first 22 years of lives probably doesn't help the issue.

[^2]:
    There's a very good CppCon talk from a Facebook dev about
    [string optimizations](https://www.youtube.com/watch?v=kPR8h4-qZdk)

[^3]: `true true false false true`
