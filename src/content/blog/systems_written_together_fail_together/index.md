---
title: Systems Written Together, Fail Together
subtitle: We're Less Diverse than you think
pubDate: 2024-11-17
tags:
  - Systems Programming
draft: true
toc: false
short: true
---

While reading Baldur Bjarnason's
[Software Crisis 2.0](https://www.baldurbjarnason.com/2021/software-crisis-2/)
I came across a disturbing figure on the success rates of software projects:

| Size     | Successful | Challenged | Failed |
| -------- | ---------- | ---------- | ------ |
| Grand    | 6%         | 51%        | 43%    |
| Large    | 11%        | 59%        | 30%    |
| Medium   | 12%        | 62%        | 26%    |
| Moderate | 24%        | 64%        | 12%    |
| Small    | 61%        | 32%        | 7%     |

Clearly, there's a crisis going on - we suck at writing good code!
