---
title: Algabraic Data Types
subtitle: It's all about the clock cycles
description: Let me think of something...
pubDate: 2022-11-26
cover: ./preview.jpg
tags:
  - Math
  - Computer-Science
draft: true
---

## Data Packing

## Yes, it's time to mention the Monads

Recall that one of the 3 main components of a full Monad is a bind function
which has the type signature

```haskell
bind :: (M a) -> (a -> M b) -> (M b)
```

So we can see now how the optional type (which recall is just a sum type between
the thing that's optional and the null state) can be very easily used through a
monad formation
