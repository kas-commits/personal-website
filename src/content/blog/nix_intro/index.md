---
title: Nix Delivers on What Docker Promised
subtitle: And why Docker made Jeff Bezos the Richest Man
description: Let me think of something...
pubDate: 2024-08-19
tags:
  - Nix
  - Docker
short: false
toc: false
draft: true
---

Docker sucks. Yea, I'm not afraid to say the quiet part aloud. Docker is an
absolute trash heap that has

I feel bad. I feel bad because over the last couple of years, after a lot of
trying (and more importantly, failing), I've discovered a super power

Stop me if you've heard this story before:

- You and your coworkers have just worked your collective butts off hitting a
  very important deadline.
- Micheal (your boss) being in a good mood decides to greenlight a new hire.
- A few weeks go by and Pete (the newguy) just got his company laptop and is
  ready to get setup.
- You happily tell him all he has to do to install the

- Agile team suceeds and gets approval for a new member
- After careful deliberation a new member is hired
- You tell new member

## Docker ain't reproducible

```docker
FROM ubuntu

ENV PRODUCTION=1

CMD hello
```

"Oh but Karim" I can hear you say, "docker has tags! You can pin your base image
to a suuuper specific tag." Okay, I'll bite, let's try pinning our container.

## Docker is too akward and brittle to "feel good"

## Nix has been around for decades?!

## Docker is the wrong abstraction (for most people)

- https://www.youtube.com/watch?v=wW9CAH9nSLs
- https://www.youtube.com/watch?v=jFrGhodqC08
- https://www.youtube.com/watch?v=3N3n9FzebAA
