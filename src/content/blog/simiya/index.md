---
title: Introducing Simiya
subtitle: Pushing "notation as a tool for thought" in a different direction
description:
  Referentially transparent type systems that can describe multi-dimensional
  arrays are still somewhat illusive to even modern programming languages, but
  researchers are building proof-of-concept compilers that could change the
  story.
pubDate: 2024-03-02
cover: ./cover.png
caption: Icon of Simiya, my new programming lanugage
tags:
  - Type Theory
  - Futhark
  - Dex
  - NumPy
  - Fortran
  - C++
draft: true
---

Hello
