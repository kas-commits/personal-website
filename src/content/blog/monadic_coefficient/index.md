---
title: 'The "Monadic" Coefficient'
pubDate: 2024-11-20
tags:
  - HPC
  - C
draft: true
short: true
---

It's no secret that I'm a functional programming girly. I've written numerous
posts about programming funcionaly including a post demonstrating an example
monad hiding in [automatic differentiation](/posts/a-gentle-introduction-to-automatic-differentiation).

The internet is filled with niche spaces. Within this higher order category
comes a sub-category - a niche within a niche, if you will - of academics and
FP developers who find it a little absurd just how tall we've built our ivory
tower. I mean, I get that luxury is form over function, but did we really need
to make the plates out of elephant tusks?

One such self-own gained enough escape velocity to reach modest popularity:

> A monad is a monoid in the category of endofunctors, what's the problem?

[The internet claims](https://news.ycombinator.com/item?id=33437394)
the original attribution comes from James Iry's 2009 blog post
[A Brief, Incomplete, and Mostly Wrong History of Programming
Languages](https://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)
and as everyone knows the age-old adage to trust everything you see on the 
internet, this must be true.

> Please do give Iry's blog post a read, it's really F*ing funny

For example take this recent tweet that blew up:

![source: https://x.com/tsoding/status/1859029510844543440](./tweet_1.png)

The discourse basically turned into a red vs blue episode (as is expected for
twitter) with academics on one side crying about how he's wrong and
non-academics on the other end
