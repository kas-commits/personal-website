---
title: Hello World!
pubDate: 2022-03-03
tags:
  - Svelte
  - Web Development
short: true
---

I know the initial commit of this repo is
[over a month old](https://gitlab.com/kas-commits/personal-website/-/commit/7adeb025e7b906c24f5743ccde8de2e738ed173a)
at this point, but a lot can happen in a month outside hobby-world. Not to
mention, that initial commit was written entirely in raw HTML, CSS and
JavaScript. Learning a framework like SvelteKit when you're still learning the
basics of web development can be... cumbersome, to say the least. I can't count
how many times I was pasting the same line of code for the tenth time, noticed
how stupid doing that is, googled it and found an infinitely more elegant
solution. The old adage really is true about copy-pasting being a code-smell.

The layout and design still needs a lot more work. Namely, I still need to
automate the process of grabbing the title, subtitle and route of a blog post. I
also need to flesh out the about and contact pages, but those are simpler tasks.
At the very least, we've got ourselves a good ol' minimum viable product going,
which is enough for a hello world.

I've documented my journey along the road and will be sure to spill the beans on
how I cobbled together this front-end in more detail, but that's for a future
blog post. For now, I simply wish to state my existence and hopefully, finally,
start producing content for the internet rather than just consuming it.

Until then, this is me signing out for the first (and hopefully not last) time.
