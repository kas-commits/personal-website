---
title: When Writing Ugly Code is Justified
subtitle: It's all about the clock cycles
description: Let me think of something...
pubDate: 2022-11-26
cover: ./cover.jpg
tags:
  - HPC
  - C
  - AVX-512
  - SIMD
draft: true
---

Do you ever look at your program and think about all the abstraction layers
between you're software and the hardware? No one else?

## The data wall is real

## Quick Refresher on the Von Neumann architecture

It's quite simple, actually:

- Pop the next CPU instruction from memory

## It's always the data, never the code

Data need not be fetched from the registers, so they do not cost any CPU clock
cycles to use. In contrast:

- Fetching data from the cache is of order $10^1$ clock cycles
- Fetching data from memory is of order $10^2$ clock cycles
- Fetching data from high speed hardware storage (think PCIe 4.0 drives) is of
  order $10^3$ clock cycles
- Fetching data from spinning hard drives is of order $10^4$ clock cycles

As you can see, any and all cache misses hurt like a mother to wait for, so it's
in every developer's best interest to avoid them as much as possible. There's a
couple of concrete strategies to help achieve this goal, but the one we're
discussing today is a form of parallelism called vectorization.

> Well, in reality vectorization is an overloaded term; in computer engineering
> it refers to automatic parallelization (the meaning I'm using in this post)
> but it also has a meaning in Math whereby a 2-D array (i.e. matrix) is
> flattened to a 1-D array (i.e. vector).

Instead of only loading the necessary data for one single operation, we can
instead load more than one iteration of the data and running the operation on
all iterations simultaneously.
