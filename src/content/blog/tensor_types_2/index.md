---
title: Hello
subtitle: A plea to improve industry tools from an error-prone human
description:
  Referentially transparent type systems that can describe multi-dimensional
  arrays are still somewhat illusive to even modern programming languages, but
  researchers are building proof-of-concept compilers that could change the
  story.
pubDate: 2023-11-24
cover: ./cover.png
caption: The Tower of Babel, book of Genisis 11:1-9
source: Pixbay
sourceLink: https://pixabay.com/vectors/tower-of-babel-bible-biblical-5783016/
tags:
  - Type Theory
  - Futhark
  - Dex
  - NumPy
  - Fortran
  - C++
draft: true
---

## Theory Will Only Take You so Far

If exposing the sizes of the tensor ranks through the type system sounds like a
great idea, why doesn't every language do it? The C++ example of using Variadic
Generics should act as a useful hint: Type systems - even ones from modern
languages -

Unfortunately, the functions we want to write as developers are often more
complex than just matrix multiplication. Seeing some more complex examples can
shed some light on the complier complexity required to support this kind of type
system.

### Types that Depend on Values

`matmul` is a good example of a simple use-case. It was simple because

1. All the size types were constants and
2. All the size types could be inferred by the inputs' _types_

But what about scenarios where the resultant type can't be inferred by the input
types? A very common example of this conundrum is the `iota` generator, which
takes an integer `n` and generates a 1D array of integers from 0 to
$n-1$[^iota]:

```
let iota : (n: i32) -> [n]i32 = 0..<n
```

Notice how, in this code, the return type `[n]i32` isn't dependent on the
input's **_type_** but rather the input's **_value_**. So you could say that the
return type is a function of the input's value, or in other words, that the
return type is a type dependent on some value. Theorists call type systems that
allow these kinds of signatures as
[Dependent Type Systems](https://en.wikipedia.org/wiki/Dependent_type), which
have usually been reserved for the most hardcore of theoretical languages like
Coq, Idris and Agda. Futhark, being a research language has taken on the mantle
of implementing dependent types and now supports the above code as valid and
compilable.

This isn't the only dependency direction we can have. Theoretically, you could
imagine writing a function that produces a value which is dependent on the type
of the input itself. If this was possible we could implement a function that
returns the length of a 1D array in a pretty trivial way:

```
let length [n] : (a: [n]f32) -> i32 = n
```

Having this ability in a type system (including the `iota` example) makes it a
$\lambda P2$ type system, which puts it high up on the
[Lambda Cube](<https://en.wikipedia.org/wiki/Lambda_cube#(%CE%BBP)_Lambda-P>)
hierarchy of type systems. [^lambda_cube_disclaimer]

### Types that Depend on Algebraic Expressions

Let's say you wanted to write a function that took multiple ND arrays and
produced a new ND array where the size of one or more dimension is an algabraic
expression of the previous sizes. A simple example would be `concat`, which
would have type signature like so:

```
val concat [n][m] : (a: [n]f32) -> (b: [m]f32) -> [n+m]f32
```

This would be great to have if possible! But how would the type system evaluate
`n+m`? `n` and `m` aren't terms - they're _types_. Well, in Futhark specifically
this can be done easily at the call site because `n` and `m` are stored as
runtime terms with type `i32`, so adding them together is
simple.[^futhark_size_type_disclaimer]

What about situations where adding them isn't so simple, though? Consider what
would happen if we wanted to write a function that batched an array along the
first axis. It would need a type signature resembling this:

```
val batch [n] 't : (x: [n]t) -> (m: i32) -> [m][n / m]t
```

However this requires a strong constraint that `m` divides `n` with no
remainder. Another point of confusion can be caused by the ambiguity of
addition. Consider a function that wanted to reshape a 1D array to a 3D one:

```
val reshape [n][m][k] : (x: [n+m+k]f32) -> [n][m][k]f32
```

How should the compiler know how to asign `n`, `m` and `k`? After all, the input
to this function will only have a single size integer. So unless we evaluate the
entire function itself the compiler has no clue how to asign the values
correctly. This brings us perfectly to our last point of contension.

### Existentially Quantified Types

We've seen many complex examples above of situations where we simply can't tell
prior to code executation what the size types should be. We've covered reshaping
and batching but even a function as simple as `filter` won't work unless we give
ourselves an escape hatch. Enter existentially quantified types.

These quantified types get their name from the root word "exists," The idea
being that there exists some size value such that the type constraints can be
properly satisfied. This differs from the usual universally quantified types
where every possible input of the correct type will always produce the same
output. In mathematical notation.

In the Dex language, we can use the following syntax to define the filter
function:

```
filter :: (a -> Bool) -> m=>a -> E n. n=>a
```

The way to read this is that, for some boolean condition function and a
collection of `a`, there exists a size type `n` such that the filter function
returns a collection of `n` elements of type `a`.

> Note: The reason for chosing E as the syntax is because it kind of looks like
> a backwards mirror of the $\exists$ math symbol for existance.

With a construct like this we can express to the type system that "hey, we don't
know what the size type of the return will be, but we know for sure that there
is some variable that will fulfill this signature."

## Compile Time Versus Object Time

Perfect is the enemy of the good. That's not just some old tale, no longer
applicable to the 21st century, but a well-intentioned, generally sane piece of
advice. Sure, there are many situations in which programming errors are much
less tolerable (the airline, banking and medical industries come to mind) but
how often do we develop that kind of software?

We mustn't forget that abstractions, including those part of a language, are
designed to make writing and composing code _easier_. If adding the full kit and
caboodle comes at the cost of shipping the feature a decade later (or not at
all!) then perhaps it hasn't earned its place in the sedimentary layer of our
core systems languages. And speaking of deadlines for shipping new features...

[^futhark_size_type_disclaimer]:
    To be specific, they're size type extensions. In reality the Futhark
    compiler considers the `n` and `m` symbols as terms with type `i32`, but
    this is very specifically a Futhark thing. Futhark has not, in general,
    implemented algabraic type expressions as valid expressions. If you
    generalize this idea to an entire type system you would have a language that
    can natively express
    [Algabraic data types](https://en.wikipedia.org/wiki/Algebraic_data_type).

[^lambda_cube_disclaimer]:
    It should be noted that in order to qualify as a $\lambda P2$ type system
    the entire type universe needs to have this dependent type principle. In
    Futhark, this principle is only applied to size type parameters, which makes
    the implementation much more straightforward and efficient to map onto GPUs,
    however it does preclude it from the title on the lambda cube.
