import rss from "@astrojs/rss";
import { getCollection } from "astro:content";

export const GET = async (context) =>
  rss({
    title: import.meta.env.BLOG_TITLE,
    description:
      "An unravelling of numbered thoughts that may hopefully unravel to a beutiful opus.",
    site: context.site,
    items: (
      await getCollection("blog", ({ data }) => {
        return data.draft !== true;
      })
    )
      .map((post) => ({
        title: post.data.title,
        pubDate: post.data.pubDate,
        description: post.data.description,
        link: `/blog/posts/${post.slug}/`,
      }))
      .sort(
        (a, b) => new Date(b.pubDate).valueOf() - new Date(a.pubDate).valueOf(),
      ),
  });
