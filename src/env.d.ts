/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

interface ImportMetaEnv {
  readonly BLOG_TITLE: string;
  readonly AUTHOR: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
