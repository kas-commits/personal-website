# Personal Website

This repository holds my personal website. It's an application written with
Astro and Tailwindcss. If you like the layout and general design feel free to
use this as a template for your own website!

## How to use

There's two main ways to use this repo. The first is during the development
cycle and the other way is to build the target. To run the app in dev mode just
run

```bash
npm run dev
```

which will incrementally compile the project using Vite then refresh the
contents and serve it on localhost. This is a super handy tool which gives you
instant feedback. The other method involves building the target, which you can
do by running

```bash
npm run build
```

This will build the app and reduce everything to html, css, javascript and some
extra resources. This should be servable by any generic engine, but we can just
use SvelteKit's engine which you can do by running

```bash
npm run preview
```

## Design

This app is designed to be statically compiled so that it can be hosted
practically anywhere. Astro Allows us to write blog posts in plain old Markdown.
This means we can write Astro code in our Markdown and everything will work
brilliantly. The true beauty, though, is that if we don't use Astro in our
Markdown files, then our blog content will be completely separated by the
website design. Having the design separate from the content.

## TODO

- [x] Use Awesomefonts for icons
- [x] Get a favicon
- [x] Setup metadata for all routes
- [x] Move to SvelteKit
- [x] Use Tailwind for styling
- [x] Create a submission form for the contact page
- [x] Have an index page where all posts are automatically sourced and shown
- [x] Use Mdsvex for blog post content
- [x] Make LaTeX available inside blog post content
- [x] Optimize usage and peformance of KaTeX
- [x] self-host all resources to avoid external HTTP requests
- [x] Finish the design of the About Page
- [x] Add a dark mode
- [x] Add code block support
- [x] Add tags to blog posts
- [x] Add an RSS feed
- [x] Move blog over to Astro + SolidJS
- [ ] Add a comment section
- [ ] Implement my own backend for handling contact submissions
